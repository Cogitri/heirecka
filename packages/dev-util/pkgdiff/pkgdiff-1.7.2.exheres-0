# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=lvc tag=${PV} ]

SUMMARY="A tool for visualizing changes in Linux software packages"
DESCRIPTION="
Package Changes Analyzer (pkgdiff) is a tool for visualizing changes in Linux
software packages (RPM, DEB, TAR.GZ, etc). The tool is intended for Linux
maintainers who are interested in ensuring compatibility of old and new
versions of packages.
"

HOMEPAGE="https://lvc.github.io/${PN}/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        app-text/wdiff
        dev-lang/perl:*
    suggestion:
        app-arch/dpkg [[ description = [ For analysis of DEB-packages ] ]]
        app-arch/rpm [[ description = [ For analysis of RPM-packages ] ]]
"

BUGS_TO="heirecka@exherbo.org"

src_prepare() {
    # Workaround for our prefix/bin/../../share
    edo sed -e "s:\(/../share/\):/..\1:" \
            -e "s:'MODULES_INSTALL_PATH':\"../../share/${PN}\":" \
            -i pkgdiff.pl
}

src_compile() {
    # There's nothing to build
    :
}

src_install() {
    edo mkdir -p "${IMAGE}"/usr/$(exhost --target)
    edo mkdir "${IMAGE}"/usr/share

    edo perl Makefile.pl -install \
        --prefix=/usr/$(exhost --target) \
        --destdir="${IMAGE}"

    edo mv "${IMAGE}"/usr/{$(exhost --target),}/share/pkgdiff
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share
}

