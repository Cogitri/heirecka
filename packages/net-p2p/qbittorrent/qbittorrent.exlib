# Copyright 2013-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.xz ] freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="qBittorrent aims to provide a Free Software alternative to µtorrent"

LICENCES="GPL-2"
SLOT="0"

MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.23]
        x11-libs/qttools:5[>=5.5.1] [[ note = [ lrelease for translations ] ]]
    build+run:
        dev-libs/boost[>=1.35]
        net-p2p/libtorrent-rasterbar[>=1.0.6]
        sys-libs/zlib[>=1.2.5.2]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        x11-libs/qtbase:5[>=5.5.1]
        x11-libs/qtsvg:5[>=5.5.1]
    run:
        dev-lang/python:=[>=2.3&<3.0]    [[ note = [ needed by the search engine ] ]]
"
# NOTE: --with-qtsingleapplication=[system|shipped] - unwritten

BUGS_TO="heirecka@exherbo.org"

# systemd only matters if no gui is built
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --prefix="${IMAGE}"/usr/$(exhost --target)
    --datadir="${IMAGE}"/usr/share
    --mandir="${IMAGE}"/usr/share/man
    --disable-systemd
    --disable-webui
    --enable-stacktrace
)

qbittorrent_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

qbittorrent_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

